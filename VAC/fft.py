import numpy as np
import matplotlib.pyplot as plt

f=np.genfromtxt('vac.dat').transpose()

lframe=500
x=f[0][:lframe]
bulk=(f[3][:lframe]+f[4][:lframe]+f[5][:lframe]+f[6][:lframe])/4.
surf=(f[1][:lframe]+f[2][:lframe]+f[7][:lframe]+f[8][:lframe])/4.

fftbulk=np.fft.fft(bulk)
fftsurf=np.fft.fft(surf)
grid=np.fft.fftfreq(lframe,x[1]-x[0])
grid=grid*1.E3*33.35641 #to cm-1

for i in range(len(grid)):
  if (grid[i] >= 0.):
    print grid[i], fftbulk[i].real, fftsurf[i].real

#plt.plot(grid,fftbulk,grid,fftsurf)
#plt.show()

