#!/usr/bin/gnuplot
set xlabel "Time (ps)"
set ylabel "# bond lengths"
set xrange [0:700]
set key left top

set term postscript eps enhanced color "Arial" 22 
set output 'bond_lengths.eps'

p 'out.dat' u 1:2 w l lw 3 lt 1 t 'surface', \
'' u 1:5 w l lw 3 lt 4 t 'bulk'


set output 'bond_lengths_filter.eps'
p 'out_filter.dat' u 1:($2+$4)/2 w l lw 3 lt 1 t 'surface', \
'' u 1:3 w l lw 3 lt 4 t 'bulk'
set term X11
