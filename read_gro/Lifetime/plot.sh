#!/bin/bash

[ -e plot.gnu ] && rm plot.gnu

cat > plot.gnu << EOF
#!/usr/bin/gnuplot
set xlabel "Average Lifetime (ps)"
set ylabel "Probability"

set term postscript eps enhanced color "arial" 18 
set output 'lifetime.eps'

EOF

idd=2
label=$(head -n 1 $1 | awk "{print \$$idd}")
echo "p '$1' u 1:$idd w l lw 3 lt $((idd-1)) t '$label', \\" >> plot.gnu

while [ "$label" != '' ]; do
  
  idd=$((idd+1))
  label=$(head -n 1 $1 | awk "{print \$$idd}")
  echo "'' u 1:$idd w l lw 3 lt $((idd-1)) t '$label', \\" >> plot.gnu

done

echo "" >> plot.gnu
echo "set term X11" >> plot.gnu

#Generate the plot
#gnuplot plot.gnu
