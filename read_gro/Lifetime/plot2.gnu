#!/usr/bin/gnuplot
set xlabel "Z axis (nm)"
set ylabel "Average lifetime (ps)"
set y2label "Number density (a.u.)"

set term postscript eps enhanced color "Arial" 22 
set output 'lifetime2.eps'

p 'lifetime.dat' u 1:2 w l lw 3 lt 1 lc rgb "red" t 'Lifetime', \
  'N_O_distribution.dat' u 1:2 w l lw 3 lt 1 lc rgb "blue" t 'N' axes x1y2, \
  '' u 1:3 w l lw 3 lt 1 lc rgb "green" t 'O' axes x1y2

set term X11

