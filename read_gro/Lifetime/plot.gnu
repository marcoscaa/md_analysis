#!/usr/bin/gnuplot
set xlabel "Time (ps)"
set ylabel "Lifetime probability distribution"
#set xrange [0:500]

set term postscript eps enhanced color "Arial" 22 
set output 'lifetime.eps'

p 'out.dat' u 1:2 w l lw 3 lt 1 t 'surface', \
'' u 1:5 w l lw 3 lt 4 t 'bulk'#, \

set term X11
