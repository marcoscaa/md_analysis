#!/usr/bin/gnuplot
set xlabel "Time (ps)"
set ylabel "OH vector auto-correlation function"
set xrange [0:200]

set term postscript eps enhanced color "Arial" 22 
set output 'rcf.eps'

p 'out_oh.dat' u 1:3 w l lw 3 lt 1 t 'Surface', \
'' u 1:5 w l lw 3 lt 4 t 'Bulk'

set term X11
