#!/usr/bin/gnuplot
set xlabel "Time (ps)"
set ylabel "Mindist - Oti-Ow (Angstrom)"

set term postscript eps enhanced color "times" 20 
set output 'Mindist.eps'

p 'Mindist_Oti_Ow_001.dat' u 1:2 w l lw 2 lt 1 lc rgb "red" notitle

set term X11
