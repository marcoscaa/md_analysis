#Run the code with
# ./make_input.sh traj.xyz
#This will generate the index files (input for the fortran executables)

######## USER-DEFINED INPUT #########
nequil=0 #Number of initial configurations to be discarded
nskip=1  #Do analysis at each nskip frames
nhist=1000 #Number of point in the histogram
zdir=3   #Direction of the surface normal (1:x,2:y,3:z)
zoffset="0.0" #Shift the number density histogram by zoffset to avoid breaking the distribution due to pbc
nlayers=5 #Number of layers for layer-resolved analysis
layers="0 3 6 9 12 15" #Layers edges in angstrom units - consistent with zdf.x code
######## END OF USER INPUT ##########

###
nat=`head -n 1 $1 | awk '{print $1}'`
nlines=`wc -l $1 | awk '{print $1}'`
nframes=$((nlines/(nat+2)))
box=`head -n 2 $1 | grep -oP '(?<=").*?(?=")' | head -n 1`
###


##Make inputs

#Number density distribution
cat << EOF > input_zdf
$nat $nframes $nequil $nskip $nhist $zdir 
$zoffset
`echo $box | awk '{print $1, $5, $9}'`
EOF

#Angle density distribution
cat << EOF > input_angle
$nat $nframes $nequil $nskip $nhist $zdir $nlayers 
$zoffset
`echo $box | awk '{print $1, $5, $9}'`
$layers
EOF
