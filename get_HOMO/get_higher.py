#!/usr/bin/python

import numpy as np

#load files
O_ti = np.genfromtxt("O.homo.dat")
O_w = np.genfromtxt("Ow.homo.dat")
time = np.genfromtxt("temp")

#Check when HOMO O_w is higher than HOMO O_ti 

for i in range(O_ti.size):
  if ( O_w[i] > O_ti[i] ): print time[i]

