#!/usr/bin/gnuplot
set xlabel "Time (ps)"
set ylabel "Energy (eV)"

set term postscript eps enhanced color "times" 20 
set output 'HOMO.eps'

p 'O.homo.dat' u 1:2 w l lw 2 lt 1 lc rgb "red" t "O (TiO2)",\
  'Ow.homo.dat' u 1:2 w l lw 2 lt 1 lc rgb "green" t "O (Water)"
# 'Hw.homo.dat2' w l lw 2 lt 1 lc rgb "black" t "H (Water)"
# 'Ti.homo.dat2' w l lw 2 lt 1 lc rgb "blue" t "Ti",\

set term X11
