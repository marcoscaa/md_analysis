#!/bin/bash

ifort -xHost -O2 get_HOMO.f90 -o get_homo.x

./get_homo.x 001_wat-k1-1.pdos > O.homo.dat2
./get_homo.x 001_wat-k2-1.pdos > Ti.homo.dat2
./get_homo.x 001_wat-k3-1.pdos > Ow.homo.dat2
./get_homo.x 001_wat-k4-1.pdos > Hw.homo.dat2

for i in *.dat2
do
  paste tmp $i > ${i/.dat2/.dat}
done

./plot.gnu
